package com.java.tutorial.model;

import com.java.tutorial.based.BaseModel;

public class Student extends BaseModel {
    private String name;

    // int, float, long, byte -> not check null
    // AccessMotifier
    private Integer age;


    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

//    public void printfBased(){
//
//        System.out.println("Id "+ id);
//        System.out.println("Birthday: "+ birthday);
//    }
}
