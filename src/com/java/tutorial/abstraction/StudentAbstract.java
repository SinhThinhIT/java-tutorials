package com.java.tutorial.abstraction;

public abstract class StudentAbstract {
    //
    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // phuong thuc truy truong
    public abstract void init(int m, int n);
    public abstract void init(int m, int n, int b);

    void init(){
        //code processing
    }
}
