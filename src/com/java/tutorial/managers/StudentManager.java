package com.java.tutorial.managers;

import com.java.tutorial.abstraction.StudentInterface;
import com.java.tutorial.based.BaseModel;
import com.java.tutorial.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentManager implements StudentInterface {

    public void init(){
        List<Student> studentList = new ArrayList<>();

        //check null - name, age != null ? nhap : break
        Student student = new Student("Do Huy", null);

        studentList.add(student);

        // printf
        showStudent(studentList);
    }

    private void showStudent(List<Student> studentList) {
        for (Student student : studentList){
            //student.printfBased();
            System.out.println("Student name: "+student.getName());
            System.out.println("Student age: "+student.getAge());
        }
    }

    @Override
    public void show() {

    }
}
