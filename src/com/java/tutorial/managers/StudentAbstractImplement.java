package com.java.tutorial.managers;

import com.java.tutorial.abstraction.StudentAbstract;
import com.java.tutorial.abstraction.StudentInterface;

public class StudentAbstractImplement extends StudentAbstract implements StudentInterface {
    @Override
    public void init(int m, int n) {
        //ghi de lai init trong StudentAbstract
    }

    @Override
    public void init(int m, int n, int a) {

    }

    @Override
    public void show() {

    }
}
